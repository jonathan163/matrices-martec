<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'filter', 'status', 'category_id'];

    /**
     * Get Slider.
     */
    public function categoria(){
        return $this->belongsTo(Category::class);
    }

     /**
     * Get Posts.
     */
    public function descripcion(){
        return $this->hasMany(ProductAttribute::class);
    }
}
