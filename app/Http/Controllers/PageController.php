<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Attribute;
use App\Product;
use App\ProductAttribute;
use App\Imports\ProductoImport;
use Maatwebsite\Excel\Facades\Excel;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $excel = Excel::toArray(new ProductoImport, 'excel/camaras HDCVI.xlsx');
        $ctg = Category::where('name', 'Camaras')->first();
        if ($ctg == null){
            $categoria = Category::create([
                'name' => 'Camaras',
            ]);
        } else {
            $categoria = $ctg;
        } 
        $prod = $excel[0];
        $filter = $excel[1];
        //Atributos del elemento 
        $keys = $prod[0];
        $keysF = $filter[0];

        foreach ($prod as $ind => $record) { 
            if ($ind != 0) {
                $product = Product::create([
                    'sku' => $record[0],
                    'description' => ($record[1] == "null") ? null : $record[1],
                    'cost' => ($record[2] == "null") ? null : $record[2],
                    'price_1' => ($record[3] == "null") ? null : $record[3],
                    'price_2' => ($record[4] == "null") ? null : $record[4],
                    'price_3' => ($record[5] == "null") ? null : $record[5],
                ]);
            }       
        } 
        //Creación de los atributos diferentes a sku, description, cost, price_1, price_2, price_3
        foreach($keys as $ind => $key){    
            if($key != $record[0] && $key != $record[1] && $key != $record[2] && $key != $record[3] && $key != $record[4] && $key != $record[5] ){
                $atbr = Attribute::where('name', $key )->first();
                if ($atbr == null){
                    $atributo = Attribute::create([
                        'name' => $key,
                        'category_id' => $categoria->id, 
                    ]);
                } else {
                    $atributo = $atbr;
                } 
                $productoAtributo = ProductAttribute::create([
                    'description' => $record[$ind],
                    'product_id' => $product->id,
                    'attribute_id' => $atributo->id,
                ]);
            }
        }
        //Actualización de los valores de filtros
        foreach ($filter as $pr => $gr) {
            foreach ($keysF as $ind => $value) {
                if ($ind != 0) {
                    //dd($keysF, $ind, $value);
                    $filtro = Attribute::find($ind);
                    $filtro->filter = 1;
                    $filtro->save();
                    //dd($filtro);
                }
            }
        }
        echo "Carga finalizada";
    }
}
