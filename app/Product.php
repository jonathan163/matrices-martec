<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['sku', 'description', 'cost', 'price_1', 'price_2', 'price_3', 'status'];

    /**
     * Get Posts.
     */
    public function atributos(){
        return $this->hasMany(ProductAttribute::class);
    }
}
