<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'product_id', 'attribute_id'];

    /**
     * Get Slider.
     */
    public function producto(){
        return $this->belongsTo(Product::class);
    }

    /**
     * Get Posts.
     */
    public function atributo(){
        return $this->belongsTo(Attribute::class);
    }
}
